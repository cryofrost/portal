<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;


/**
 * appdevUrlGenerator
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appdevUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    static private $declaredRouteNames = array(
       '_wdt' => true,
       '_profiler_search' => true,
       '_profiler_purge' => true,
       '_profiler_import' => true,
       '_profiler_export' => true,
       '_profiler_search_results' => true,
       '_profiler' => true,
       '_configurator_home' => true,
       '_configurator_step' => true,
       '_configurator_final' => true,
       'BackendGameBundle_homepage' => true,
       'fos_user_security_login' => true,
       'fos_user_security_check' => true,
       'fos_user_security_logout' => true,
       'sonata_user_profile_show' => true,
       'sonata_user_profile_edit_authentication' => true,
       'sonata_user_profile_edit' => true,
       'fos_user_registration_register' => true,
       'fos_user_registration_check_email' => true,
       'fos_user_registration_confirm' => true,
       'fos_user_registration_confirmed' => true,
       'fos_user_resetting_request' => true,
       'fos_user_resetting_send_email' => true,
       'fos_user_resetting_check_email' => true,
       'fos_user_resetting_reset' => true,
       'fos_user_change_password' => true,
       'sonata_admin_dashboard' => true,
       'sonata_admin_retrieve_form_element' => true,
       'sonata_admin_append_form_element' => true,
       'sonata_admin_short_object_information' => true,
       'sonata_admin_set_object_field_value' => true,
       'admin_backend_user_user_list' => true,
       'admin_backend_user_user_create' => true,
       'admin_backend_user_user_batch' => true,
       'admin_backend_user_user_edit' => true,
       'admin_backend_user_user_delete' => true,
       'admin_backend_user_user_show' => true,
       'admin_backend_user_user_export' => true,
       'admin_backend_user_group_list' => true,
       'admin_backend_user_group_create' => true,
       'admin_backend_user_group_batch' => true,
       'admin_backend_user_group_edit' => true,
       'admin_backend_user_group_delete' => true,
       'admin_backend_user_group_show' => true,
       'admin_backend_user_group_export' => true,
       'admin_backend_game_game_list' => true,
       'admin_backend_game_game_create' => true,
       'admin_backend_game_game_batch' => true,
       'admin_backend_game_game_edit' => true,
       'admin_backend_game_game_delete' => true,
       'admin_backend_game_game_show' => true,
       'admin_backend_game_game_export' => true,
       'admin_backend_game_developer_list' => true,
       'admin_backend_game_developer_create' => true,
       'admin_backend_game_developer_batch' => true,
       'admin_backend_game_developer_edit' => true,
       'admin_backend_game_developer_delete' => true,
       'admin_backend_game_developer_show' => true,
       'admin_backend_game_developer_export' => true,
       'admin_backend_game_publisher_list' => true,
       'admin_backend_game_publisher_create' => true,
       'admin_backend_game_publisher_batch' => true,
       'admin_backend_game_publisher_edit' => true,
       'admin_backend_game_publisher_delete' => true,
       'admin_backend_game_publisher_show' => true,
       'admin_backend_game_publisher_export' => true,
       'admin_backend_game_localizer_list' => true,
       'admin_backend_game_localizer_create' => true,
       'admin_backend_game_localizer_batch' => true,
       'admin_backend_game_localizer_edit' => true,
       'admin_backend_game_localizer_delete' => true,
       'admin_backend_game_localizer_show' => true,
       'admin_backend_game_localizer_export' => true,
       'admin_backend_game_platform_list' => true,
       'admin_backend_game_platform_create' => true,
       'admin_backend_game_platform_batch' => true,
       'admin_backend_game_platform_edit' => true,
       'admin_backend_game_platform_delete' => true,
       'admin_backend_game_platform_show' => true,
       'admin_backend_game_platform_export' => true,
       'admin_backend_game_genre_list' => true,
       'admin_backend_game_genre_create' => true,
       'admin_backend_game_genre_batch' => true,
       'admin_backend_game_genre_edit' => true,
       'admin_backend_game_genre_delete' => true,
       'admin_backend_game_genre_show' => true,
       'admin_backend_game_genre_export' => true,
       'sonata_user_admin_security_login' => true,
       'sonata_user_admin_security_check' => true,
       'sonata_user_admin_security_logout' => true,
       'homepage' => true,
       'revert_changeset' => true,
    );

    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function generate($name, $parameters = array(), $absolute = false)
    {
        if (!isset(self::$declaredRouteNames[$name])) {
            throw new RouteNotFoundException(sprintf('Route "%s" does not exist.', $name));
        }

        $escapedName = str_replace('.', '__', $name);

        list($variables, $defaults, $requirements, $tokens) = $this->{'get'.$escapedName.'RouteInfo'}();

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $absolute);
    }

    private function get_wdtRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::toolbarAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'token',  ),  1 =>   array (    0 => 'text',    1 => '/_wdt',  ),));
    }

    private function get_profiler_searchRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::searchAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_profiler/search',  ),));
    }

    private function get_profiler_purgeRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::purgeAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_profiler/purge',  ),));
    }

    private function get_profiler_importRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::importAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_profiler/import',  ),));
    }

    private function get_profiler_exportRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::exportAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '.txt',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/\\.]+?',    3 => 'token',  ),  2 =>   array (    0 => 'text',    1 => '/_profiler/export',  ),));
    }

    private function get_profiler_search_resultsRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::searchResultsAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/search/results',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'token',  ),  2 =>   array (    0 => 'text',    1 => '/_profiler',  ),));
    }

    private function get_profilerRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::panelAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'token',  ),  1 =>   array (    0 => 'text',    1 => '/_profiler',  ),));
    }

    private function get_configurator_homeRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_configurator/',  ),));
    }

    private function get_configurator_stepRouteInfo()
    {
        return array(array (  0 => 'index',), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'index',  ),  1 =>   array (    0 => 'text',    1 => '/_configurator/step',  ),));
    }

    private function get_configurator_finalRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_configurator/final',  ),));
    }

    private function getBackendGameBundle_homepageRouteInfo()
    {
        return array(array (  0 => 'name',), array (  '_controller' => 'Backend\\GameBundle\\Controller\\DefaultController::indexAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'name',  ),  1 =>   array (    0 => 'text',    1 => '/hello',  ),));
    }

    private function getfos_user_security_loginRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/login',  ),));
    }

    private function getfos_user_security_checkRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/login_check',  ),));
    }

    private function getfos_user_security_logoutRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/logout',  ),));
    }

    private function getsonata_user_profile_showRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\UserBundle\\Controller\\ProfileController::showAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'text',    1 => '/profile/',  ),));
    }

    private function getsonata_user_profile_edit_authenticationRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\UserBundle\\Controller\\ProfileController::editAuthenticationAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/profile/edit-authentication',  ),));
    }

    private function getsonata_user_profile_editRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\UserBundle\\Controller\\ProfileController::editProfileAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/profile/edit-profile',  ),));
    }

    private function getfos_user_registration_registerRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/register/',  ),));
    }

    private function getfos_user_registration_check_emailRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'text',    1 => '/register/check-email',  ),));
    }

    private function getfos_user_registration_confirmRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'token',  ),  1 =>   array (    0 => 'text',    1 => '/register/confirm',  ),));
    }

    private function getfos_user_registration_confirmedRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'text',    1 => '/register/confirmed',  ),));
    }

    private function getfos_user_resetting_requestRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'text',    1 => '/resetting/request',  ),));
    }

    private function getfos_user_resetting_send_emailRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',), array (  '_method' => 'POST',), array (  0 =>   array (    0 => 'text',    1 => '/resetting/send-email',  ),));
    }

    private function getfos_user_resetting_check_emailRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'text',    1 => '/resetting/check-email',  ),));
    }

    private function getfos_user_resetting_resetRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',), array (  '_method' => 'GET|POST',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'token',  ),  1 =>   array (    0 => 'text',    1 => '/resetting/reset',  ),));
    }

    private function getfos_user_change_passwordRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',), array (  '_method' => 'GET|POST',), array (  0 =>   array (    0 => 'text',    1 => '/change-password/change-password',  ),));
    }

    private function getsonata_admin_dashboardRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::dashboardAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/dashboard',  ),));
    }

    private function getsonata_admin_retrieve_form_elementRouteInfo()
    {
        return array(array (), array (  '_controller' => 'sonata.admin.controller.admin:retrieveFormFieldElementAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/core/get-form-field-element',  ),));
    }

    private function getsonata_admin_append_form_elementRouteInfo()
    {
        return array(array (), array (  '_controller' => 'sonata.admin.controller.admin:appendFormFieldElementAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/core/append-form-field-element',  ),));
    }

    private function getsonata_admin_short_object_informationRouteInfo()
    {
        return array(array (), array (  '_controller' => 'sonata.admin.controller.admin:getShortObjectDescriptionAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/core/get-short-object-description',  ),));
    }

    private function getsonata_admin_set_object_field_valueRouteInfo()
    {
        return array(array (), array (  '_controller' => 'sonata.admin.controller.admin:setObjectFieldValueAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/core/set-object-field-value',  ),));
    }

    private function getadmin_backend_user_user_listRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_backend_user_user_list',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/user/user/list',  ),));
    }

    private function getadmin_backend_user_user_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_backend_user_user_create',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/user/user/create',  ),));
    }

    private function getadmin_backend_user_user_batchRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_backend_user_user_batch',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/user/user/batch',  ),));
    }

    private function getadmin_backend_user_user_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_backend_user_user_edit',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/backend/user/user',  ),));
    }

    private function getadmin_backend_user_user_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_backend_user_user_delete',), array (), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/backend/user/user',  ),));
    }

    private function getadmin_backend_user_user_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_backend_user_user_show',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/backend/user/user',  ),));
    }

    private function getadmin_backend_user_user_exportRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_backend_user_user_export',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/user/user/export',  ),));
    }

    private function getadmin_backend_user_group_listRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_backend_user_group_list',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/user/group/list',  ),));
    }

    private function getadmin_backend_user_group_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_backend_user_group_create',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/user/group/create',  ),));
    }

    private function getadmin_backend_user_group_batchRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_backend_user_group_batch',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/user/group/batch',  ),));
    }

    private function getadmin_backend_user_group_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_backend_user_group_edit',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/backend/user/group',  ),));
    }

    private function getadmin_backend_user_group_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_backend_user_group_delete',), array (), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/backend/user/group',  ),));
    }

    private function getadmin_backend_user_group_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_backend_user_group_show',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/backend/user/group',  ),));
    }

    private function getadmin_backend_user_group_exportRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_backend_user_group_export',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/user/group/export',  ),));
    }

    private function getadmin_backend_game_game_listRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.game.admin.game',  '_sonata_name' => 'admin_backend_game_game_list',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/game/game/list',  ),));
    }

    private function getadmin_backend_game_game_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.game.admin.game',  '_sonata_name' => 'admin_backend_game_game_create',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/game/game/create',  ),));
    }

    private function getadmin_backend_game_game_batchRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.game.admin.game',  '_sonata_name' => 'admin_backend_game_game_batch',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/game/game/batch',  ),));
    }

    private function getadmin_backend_game_game_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.game.admin.game',  '_sonata_name' => 'admin_backend_game_game_edit',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/backend/game/game',  ),));
    }

    private function getadmin_backend_game_game_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.game.admin.game',  '_sonata_name' => 'admin_backend_game_game_delete',), array (), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/backend/game/game',  ),));
    }

    private function getadmin_backend_game_game_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.game.admin.game',  '_sonata_name' => 'admin_backend_game_game_show',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/backend/game/game',  ),));
    }

    private function getadmin_backend_game_game_exportRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.game.admin.game',  '_sonata_name' => 'admin_backend_game_game_export',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/game/game/export',  ),));
    }

    private function getadmin_backend_game_developer_listRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.game.admin.developer',  '_sonata_name' => 'admin_backend_game_developer_list',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/game/developer/list',  ),));
    }

    private function getadmin_backend_game_developer_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.game.admin.developer',  '_sonata_name' => 'admin_backend_game_developer_create',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/game/developer/create',  ),));
    }

    private function getadmin_backend_game_developer_batchRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.game.admin.developer',  '_sonata_name' => 'admin_backend_game_developer_batch',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/game/developer/batch',  ),));
    }

    private function getadmin_backend_game_developer_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.game.admin.developer',  '_sonata_name' => 'admin_backend_game_developer_edit',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/backend/game/developer',  ),));
    }

    private function getadmin_backend_game_developer_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.game.admin.developer',  '_sonata_name' => 'admin_backend_game_developer_delete',), array (), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/backend/game/developer',  ),));
    }

    private function getadmin_backend_game_developer_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.game.admin.developer',  '_sonata_name' => 'admin_backend_game_developer_show',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/backend/game/developer',  ),));
    }

    private function getadmin_backend_game_developer_exportRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.game.admin.developer',  '_sonata_name' => 'admin_backend_game_developer_export',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/game/developer/export',  ),));
    }

    private function getadmin_backend_game_publisher_listRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.game.admin.publisher',  '_sonata_name' => 'admin_backend_game_publisher_list',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/game/publisher/list',  ),));
    }

    private function getadmin_backend_game_publisher_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.game.admin.publisher',  '_sonata_name' => 'admin_backend_game_publisher_create',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/game/publisher/create',  ),));
    }

    private function getadmin_backend_game_publisher_batchRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.game.admin.publisher',  '_sonata_name' => 'admin_backend_game_publisher_batch',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/game/publisher/batch',  ),));
    }

    private function getadmin_backend_game_publisher_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.game.admin.publisher',  '_sonata_name' => 'admin_backend_game_publisher_edit',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/backend/game/publisher',  ),));
    }

    private function getadmin_backend_game_publisher_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.game.admin.publisher',  '_sonata_name' => 'admin_backend_game_publisher_delete',), array (), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/backend/game/publisher',  ),));
    }

    private function getadmin_backend_game_publisher_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.game.admin.publisher',  '_sonata_name' => 'admin_backend_game_publisher_show',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/backend/game/publisher',  ),));
    }

    private function getadmin_backend_game_publisher_exportRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.game.admin.publisher',  '_sonata_name' => 'admin_backend_game_publisher_export',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/game/publisher/export',  ),));
    }

    private function getadmin_backend_game_localizer_listRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.game.admin.localizer',  '_sonata_name' => 'admin_backend_game_localizer_list',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/game/localizer/list',  ),));
    }

    private function getadmin_backend_game_localizer_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.game.admin.localizer',  '_sonata_name' => 'admin_backend_game_localizer_create',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/game/localizer/create',  ),));
    }

    private function getadmin_backend_game_localizer_batchRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.game.admin.localizer',  '_sonata_name' => 'admin_backend_game_localizer_batch',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/game/localizer/batch',  ),));
    }

    private function getadmin_backend_game_localizer_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.game.admin.localizer',  '_sonata_name' => 'admin_backend_game_localizer_edit',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/backend/game/localizer',  ),));
    }

    private function getadmin_backend_game_localizer_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.game.admin.localizer',  '_sonata_name' => 'admin_backend_game_localizer_delete',), array (), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/backend/game/localizer',  ),));
    }

    private function getadmin_backend_game_localizer_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.game.admin.localizer',  '_sonata_name' => 'admin_backend_game_localizer_show',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/backend/game/localizer',  ),));
    }

    private function getadmin_backend_game_localizer_exportRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.game.admin.localizer',  '_sonata_name' => 'admin_backend_game_localizer_export',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/game/localizer/export',  ),));
    }

    private function getadmin_backend_game_platform_listRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.game.admin.platform',  '_sonata_name' => 'admin_backend_game_platform_list',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/game/platform/list',  ),));
    }

    private function getadmin_backend_game_platform_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.game.admin.platform',  '_sonata_name' => 'admin_backend_game_platform_create',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/game/platform/create',  ),));
    }

    private function getadmin_backend_game_platform_batchRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.game.admin.platform',  '_sonata_name' => 'admin_backend_game_platform_batch',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/game/platform/batch',  ),));
    }

    private function getadmin_backend_game_platform_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.game.admin.platform',  '_sonata_name' => 'admin_backend_game_platform_edit',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/backend/game/platform',  ),));
    }

    private function getadmin_backend_game_platform_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.game.admin.platform',  '_sonata_name' => 'admin_backend_game_platform_delete',), array (), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/backend/game/platform',  ),));
    }

    private function getadmin_backend_game_platform_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.game.admin.platform',  '_sonata_name' => 'admin_backend_game_platform_show',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/backend/game/platform',  ),));
    }

    private function getadmin_backend_game_platform_exportRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.game.admin.platform',  '_sonata_name' => 'admin_backend_game_platform_export',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/game/platform/export',  ),));
    }

    private function getadmin_backend_game_genre_listRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.game.admin.genre',  '_sonata_name' => 'admin_backend_game_genre_list',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/game/genre/list',  ),));
    }

    private function getadmin_backend_game_genre_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.game.admin.genre',  '_sonata_name' => 'admin_backend_game_genre_create',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/game/genre/create',  ),));
    }

    private function getadmin_backend_game_genre_batchRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.game.admin.genre',  '_sonata_name' => 'admin_backend_game_genre_batch',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/game/genre/batch',  ),));
    }

    private function getadmin_backend_game_genre_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.game.admin.genre',  '_sonata_name' => 'admin_backend_game_genre_edit',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/backend/game/genre',  ),));
    }

    private function getadmin_backend_game_genre_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.game.admin.genre',  '_sonata_name' => 'admin_backend_game_genre_delete',), array (), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/backend/game/genre',  ),));
    }

    private function getadmin_backend_game_genre_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.game.admin.genre',  '_sonata_name' => 'admin_backend_game_genre_show',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/backend/game/genre',  ),));
    }

    private function getadmin_backend_game_genre_exportRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.game.admin.genre',  '_sonata_name' => 'admin_backend_game_genre_export',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/backend/game/genre/export',  ),));
    }

    private function getsonata_user_admin_security_loginRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\UserBundle\\Controller\\AdminSecurityController::loginAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/login',  ),));
    }

    private function getsonata_user_admin_security_checkRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\UserBundle\\Controller\\AdminSecurityController::checkAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/login_check',  ),));
    }

    private function getsonata_user_admin_security_logoutRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\UserBundle\\Controller\\AdminSecurityController::logoutAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/logout',  ),));
    }

    private function gethomepageRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::dashboardAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/',  ),));
    }

    private function getrevert_changesetRouteInfo()
    {
        return array(array (  0 => 'entity',  1 => 'id',  2 => 'version',), array (  '_controller' => 'Backend\\UserBundle\\Controller\\RevertController::revertAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'version',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'entity',  ),  3 =>   array (    0 => 'text',    1 => '/admin/revert',  ),));
    }
}
