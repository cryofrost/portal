<?php

/* SonataAdminBundle:CRUD:base_list_field.html.twig */
class __TwigTemplate_71afc9fd8dbb91cc29f48c15a3139713 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 11
        echo "
<td class=\"sonata-ba-list-field sonata-ba-list-field-";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "field_description"), "type"), "html", null, true);
        echo "\" objectId=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "admin"), "id", array(0 => $this->getContext($context, "object")), "method"), "html", null, true);
        echo "\">
    ";
        // line 13
        if (((($this->getAttribute($this->getAttribute($this->getContext($context, "field_description", true), "options", array(), "any", false, true), "identifier", array(), "any", true, true) && $this->getAttribute($this->getAttribute($this->getContext($context, "field_description", true), "options", array(), "any", false, true), "route", array(), "any", true, true)) && $this->getAttribute($this->getContext($context, "admin"), "isGranted", array(0 => ((($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "field_description"), "options"), "route"), "name") == "show")) ? ("VIEW") : (twig_upper_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "field_description"), "options"), "route"), "name")))), 1 => $this->getContext($context, "object")), "method")) && $this->getAttribute($this->getContext($context, "admin"), "hasRoute", array(0 => $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "field_description"), "options"), "route"), "name")), "method"))) {
            // line 19
            echo "        <a href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "admin"), "generateObjectUrl", array(0 => $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "field_description"), "options"), "route"), "name"), 1 => $this->getContext($context, "object"), 2 => $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "field_description"), "options"), "route"), "parameters")), "method"), "html", null, true);
            echo "\">";
            // line 20
            $this->displayBlock('field', $context, $blocks);
            // line 21
            echo "</a>
    ";
        } else {
            // line 23
            echo "        ";
            $this->displayBlock("field", $context, $blocks);
            echo "
    ";
        }
        // line 25
        echo "</td>
";
    }

    // line 20
    public function block_field($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getContext($context, "value"), "html", null, true);
    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:base_list_field.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 25,  39 => 23,  35 => 21,  29 => 19,  27 => 13,  18 => 11,  21 => 12,  17 => 11,  630 => 200,  627 => 199,  622 => 146,  618 => 143,  612 => 142,  608 => 140,  604 => 138,  599 => 137,  596 => 136,  593 => 135,  588 => 134,  581 => 104,  578 => 103,  570 => 100,  564 => 99,  556 => 97,  544 => 93,  541 => 92,  536 => 91,  531 => 89,  528 => 88,  521 => 83,  510 => 81,  507 => 80,  501 => 51,  497 => 50,  493 => 49,  488 => 47,  484 => 46,  480 => 45,  472 => 43,  468 => 42,  464 => 41,  459 => 39,  455 => 38,  450 => 37,  447 => 36,  437 => 32,  431 => 29,  426 => 27,  423 => 26,  420 => 25,  411 => 204,  409 => 199,  406 => 198,  400 => 195,  394 => 192,  391 => 191,  389 => 190,  386 => 189,  380 => 187,  378 => 186,  375 => 185,  367 => 182,  364 => 181,  352 => 176,  346 => 174,  344 => 173,  339 => 170,  333 => 167,  328 => 165,  320 => 161,  317 => 160,  303 => 159,  297 => 157,  294 => 156,  276 => 155,  270 => 153,  264 => 150,  262 => 149,  258 => 147,  256 => 146,  250 => 133,  247 => 132,  243 => 130,  234 => 127,  231 => 126,  203 => 121,  200 => 120,  182 => 119,  179 => 118,  174 => 116,  172 => 115,  164 => 109,  158 => 107,  152 => 88,  143 => 79,  126 => 70,  120 => 68,  103 => 66,  99 => 64,  96 => 63,  76 => 61,  73 => 60,  67 => 58,  60 => 55,  54 => 36,  51 => 35,  49 => 25,  38 => 18,  34 => 16,  32 => 15,  30 => 14,  28 => 13,  26 => 12,  24 => 11,  585 => 133,  580 => 197,  574 => 195,  572 => 194,  566 => 190,  557 => 187,  553 => 96,  549 => 95,  542 => 184,  538 => 183,  533 => 90,  526 => 179,  518 => 177,  515 => 82,  512 => 175,  506 => 162,  502 => 160,  500 => 159,  494 => 155,  483 => 153,  479 => 152,  476 => 44,  473 => 150,  461 => 138,  458 => 137,  453 => 164,  451 => 150,  446 => 147,  444 => 137,  441 => 33,  438 => 135,  434 => 131,  425 => 124,  417 => 122,  415 => 121,  412 => 120,  404 => 118,  402 => 117,  399 => 116,  393 => 115,  385 => 113,  377 => 111,  374 => 110,  369 => 183,  366 => 107,  358 => 179,  356 => 178,  353 => 103,  345 => 101,  343 => 100,  335 => 95,  332 => 94,  330 => 166,  325 => 164,  323 => 89,  318 => 86,  315 => 85,  301 => 84,  292 => 83,  275 => 82,  271 => 81,  268 => 152,  266 => 79,  259 => 77,  255 => 76,  252 => 144,  249 => 74,  244 => 71,  237 => 128,  228 => 67,  224 => 66,  221 => 65,  217 => 125,  214 => 63,  211 => 123,  205 => 58,  199 => 57,  196 => 56,  192 => 54,  188 => 53,  183 => 52,  177 => 117,  165 => 50,  163 => 49,  160 => 48,  157 => 47,  154 => 105,  151 => 45,  148 => 86,  145 => 80,  142 => 42,  139 => 41,  136 => 74,  130 => 72,  127 => 35,  123 => 69,  119 => 32,  116 => 31,  110 => 171,  108 => 170,  105 => 169,  101 => 167,  98 => 166,  95 => 135,  93 => 134,  89 => 132,  87 => 74,  84 => 73,  82 => 62,  79 => 62,  77 => 31,  71 => 29,  68 => 28,  65 => 57,  62 => 56,  56 => 53,  50 => 20,  42 => 20,  40 => 19,  36 => 17,  33 => 20,);
    }
}
