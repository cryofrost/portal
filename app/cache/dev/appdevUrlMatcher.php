<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appdevUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appdevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = urldecode($pathinfo);

        // _wdt
        if (preg_match('#^/_wdt/(?P<token>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::toolbarAction',)), array('_route' => '_wdt'));
        }

        if (0 === strpos($pathinfo, '/_profiler')) {
            // _profiler_search
            if ($pathinfo === '/_profiler/search') {
                return array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::searchAction',  '_route' => '_profiler_search',);
            }

            // _profiler_purge
            if ($pathinfo === '/_profiler/purge') {
                return array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::purgeAction',  '_route' => '_profiler_purge',);
            }

            // _profiler_import
            if ($pathinfo === '/_profiler/import') {
                return array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::importAction',  '_route' => '_profiler_import',);
            }

            // _profiler_export
            if (0 === strpos($pathinfo, '/_profiler/export') && preg_match('#^/_profiler/export/(?P<token>[^/\\.]+?)\\.txt$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::exportAction',)), array('_route' => '_profiler_export'));
            }

            // _profiler_search_results
            if (preg_match('#^/_profiler/(?P<token>[^/]+?)/search/results$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::searchResultsAction',)), array('_route' => '_profiler_search_results'));
            }

            // _profiler
            if (preg_match('#^/_profiler/(?P<token>[^/]+?)$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::panelAction',)), array('_route' => '_profiler'));
            }

        }

        if (0 === strpos($pathinfo, '/_configurator')) {
            // _configurator_home
            if (rtrim($pathinfo, '/') === '/_configurator') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', '_configurator_home');
                }
                return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
            }

            // _configurator_step
            if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]+?)$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',)), array('_route' => '_configurator_step'));
            }

            // _configurator_final
            if ($pathinfo === '/_configurator/final') {
                return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
            }

        }

        // BackendGameBundle_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Backend\\GameBundle\\Controller\\DefaultController::indexAction',)), array('_route' => 'BackendGameBundle_homepage'));
        }

        // fos_user_security_login
        if ($pathinfo === '/login') {
            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
        }

        // fos_user_security_check
        if ($pathinfo === '/login_check') {
            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
        }

        // fos_user_security_logout
        if ($pathinfo === '/logout') {
            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
        }

        if (0 === strpos($pathinfo, '/profile')) {
            // sonata_user_profile_show
            if (rtrim($pathinfo, '/') === '/profile') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_sonata_user_profile_show;
                }
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'sonata_user_profile_show');
                }
                return array (  '_controller' => 'Sonata\\UserBundle\\Controller\\ProfileController::showAction',  '_route' => 'sonata_user_profile_show',);
            }
            not_sonata_user_profile_show:

            // sonata_user_profile_edit_authentication
            if ($pathinfo === '/profile/edit-authentication') {
                return array (  '_controller' => 'Sonata\\UserBundle\\Controller\\ProfileController::editAuthenticationAction',  '_route' => 'sonata_user_profile_edit_authentication',);
            }

            // sonata_user_profile_edit
            if ($pathinfo === '/profile/edit-profile') {
                return array (  '_controller' => 'Sonata\\UserBundle\\Controller\\ProfileController::editProfileAction',  '_route' => 'sonata_user_profile_edit',);
            }

        }

        if (0 === strpos($pathinfo, '/register')) {
            // fos_user_registration_register
            if (rtrim($pathinfo, '/') === '/register') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fos_user_registration_register');
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  '_route' => 'fos_user_registration_register',);
            }

            // fos_user_registration_check_email
            if ($pathinfo === '/register/check-email') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_registration_check_email;
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
            }
            not_fos_user_registration_check_email:

            // fos_user_registration_confirm
            if (0 === strpos($pathinfo, '/register/confirm') && preg_match('#^/register/confirm/(?P<token>[^/]+?)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_registration_confirm;
                }
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',)), array('_route' => 'fos_user_registration_confirm'));
            }
            not_fos_user_registration_confirm:

            // fos_user_registration_confirmed
            if ($pathinfo === '/register/confirmed') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_registration_confirmed;
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
            }
            not_fos_user_registration_confirmed:

        }

        if (0 === strpos($pathinfo, '/resetting')) {
            // fos_user_resetting_request
            if ($pathinfo === '/resetting/request') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_resetting_request;
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_route' => 'fos_user_resetting_request',);
            }
            not_fos_user_resetting_request:

            // fos_user_resetting_send_email
            if ($pathinfo === '/resetting/send-email') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_fos_user_resetting_send_email;
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
            }
            not_fos_user_resetting_send_email:

            // fos_user_resetting_check_email
            if ($pathinfo === '/resetting/check-email') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_resetting_check_email;
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
            }
            not_fos_user_resetting_check_email:

            // fos_user_resetting_reset
            if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]+?)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_resetting_reset;
                }
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',)), array('_route' => 'fos_user_resetting_reset'));
            }
            not_fos_user_resetting_reset:

        }

        // fos_user_change_password
        if ($pathinfo === '/change-password/change-password') {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_fos_user_change_password;
            }
            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_route' => 'fos_user_change_password',);
        }
        not_fos_user_change_password:

        if (0 === strpos($pathinfo, '/admin')) {
            // sonata_admin_dashboard
            if ($pathinfo === '/admin/dashboard') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::dashboardAction',  '_route' => 'sonata_admin_dashboard',);
            }

            // sonata_admin_retrieve_form_element
            if ($pathinfo === '/admin/core/get-form-field-element') {
                return array (  '_controller' => 'sonata.admin.controller.admin:retrieveFormFieldElementAction',  '_route' => 'sonata_admin_retrieve_form_element',);
            }

            // sonata_admin_append_form_element
            if ($pathinfo === '/admin/core/append-form-field-element') {
                return array (  '_controller' => 'sonata.admin.controller.admin:appendFormFieldElementAction',  '_route' => 'sonata_admin_append_form_element',);
            }

            // sonata_admin_short_object_information
            if ($pathinfo === '/admin/core/get-short-object-description') {
                return array (  '_controller' => 'sonata.admin.controller.admin:getShortObjectDescriptionAction',  '_route' => 'sonata_admin_short_object_information',);
            }

            // sonata_admin_set_object_field_value
            if ($pathinfo === '/admin/core/set-object-field-value') {
                return array (  '_controller' => 'sonata.admin.controller.admin:setObjectFieldValueAction',  '_route' => 'sonata_admin_set_object_field_value',);
            }

            // admin_backend_user_user_list
            if ($pathinfo === '/admin/backend/user/user/list') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_backend_user_user_list',  '_route' => 'admin_backend_user_user_list',);
            }

            // admin_backend_user_user_create
            if ($pathinfo === '/admin/backend/user/user/create') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_backend_user_user_create',  '_route' => 'admin_backend_user_user_create',);
            }

            // admin_backend_user_user_batch
            if ($pathinfo === '/admin/backend/user/user/batch') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_backend_user_user_batch',  '_route' => 'admin_backend_user_user_batch',);
            }

            // admin_backend_user_user_edit
            if (0 === strpos($pathinfo, '/admin/backend/user/user') && preg_match('#^/admin/backend/user/user/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_backend_user_user_edit',)), array('_route' => 'admin_backend_user_user_edit'));
            }

            // admin_backend_user_user_delete
            if (0 === strpos($pathinfo, '/admin/backend/user/user') && preg_match('#^/admin/backend/user/user/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_backend_user_user_delete',)), array('_route' => 'admin_backend_user_user_delete'));
            }

            // admin_backend_user_user_show
            if (0 === strpos($pathinfo, '/admin/backend/user/user') && preg_match('#^/admin/backend/user/user/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_backend_user_user_show',)), array('_route' => 'admin_backend_user_user_show'));
            }

            // admin_backend_user_user_export
            if ($pathinfo === '/admin/backend/user/user/export') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_backend_user_user_export',  '_route' => 'admin_backend_user_user_export',);
            }

            // admin_backend_user_group_list
            if ($pathinfo === '/admin/backend/user/group/list') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_backend_user_group_list',  '_route' => 'admin_backend_user_group_list',);
            }

            // admin_backend_user_group_create
            if ($pathinfo === '/admin/backend/user/group/create') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_backend_user_group_create',  '_route' => 'admin_backend_user_group_create',);
            }

            // admin_backend_user_group_batch
            if ($pathinfo === '/admin/backend/user/group/batch') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_backend_user_group_batch',  '_route' => 'admin_backend_user_group_batch',);
            }

            // admin_backend_user_group_edit
            if (0 === strpos($pathinfo, '/admin/backend/user/group') && preg_match('#^/admin/backend/user/group/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_backend_user_group_edit',)), array('_route' => 'admin_backend_user_group_edit'));
            }

            // admin_backend_user_group_delete
            if (0 === strpos($pathinfo, '/admin/backend/user/group') && preg_match('#^/admin/backend/user/group/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_backend_user_group_delete',)), array('_route' => 'admin_backend_user_group_delete'));
            }

            // admin_backend_user_group_show
            if (0 === strpos($pathinfo, '/admin/backend/user/group') && preg_match('#^/admin/backend/user/group/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_backend_user_group_show',)), array('_route' => 'admin_backend_user_group_show'));
            }

            // admin_backend_user_group_export
            if ($pathinfo === '/admin/backend/user/group/export') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_backend_user_group_export',  '_route' => 'admin_backend_user_group_export',);
            }

            // admin_backend_game_game_list
            if ($pathinfo === '/admin/backend/game/game/list') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.game.admin.game',  '_sonata_name' => 'admin_backend_game_game_list',  '_route' => 'admin_backend_game_game_list',);
            }

            // admin_backend_game_game_create
            if ($pathinfo === '/admin/backend/game/game/create') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.game.admin.game',  '_sonata_name' => 'admin_backend_game_game_create',  '_route' => 'admin_backend_game_game_create',);
            }

            // admin_backend_game_game_batch
            if ($pathinfo === '/admin/backend/game/game/batch') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.game.admin.game',  '_sonata_name' => 'admin_backend_game_game_batch',  '_route' => 'admin_backend_game_game_batch',);
            }

            // admin_backend_game_game_edit
            if (0 === strpos($pathinfo, '/admin/backend/game/game') && preg_match('#^/admin/backend/game/game/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.game.admin.game',  '_sonata_name' => 'admin_backend_game_game_edit',)), array('_route' => 'admin_backend_game_game_edit'));
            }

            // admin_backend_game_game_delete
            if (0 === strpos($pathinfo, '/admin/backend/game/game') && preg_match('#^/admin/backend/game/game/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.game.admin.game',  '_sonata_name' => 'admin_backend_game_game_delete',)), array('_route' => 'admin_backend_game_game_delete'));
            }

            // admin_backend_game_game_show
            if (0 === strpos($pathinfo, '/admin/backend/game/game') && preg_match('#^/admin/backend/game/game/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.game.admin.game',  '_sonata_name' => 'admin_backend_game_game_show',)), array('_route' => 'admin_backend_game_game_show'));
            }

            // admin_backend_game_game_export
            if ($pathinfo === '/admin/backend/game/game/export') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.game.admin.game',  '_sonata_name' => 'admin_backend_game_game_export',  '_route' => 'admin_backend_game_game_export',);
            }

            // admin_backend_game_developer_list
            if ($pathinfo === '/admin/backend/game/developer/list') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.game.admin.developer',  '_sonata_name' => 'admin_backend_game_developer_list',  '_route' => 'admin_backend_game_developer_list',);
            }

            // admin_backend_game_developer_create
            if ($pathinfo === '/admin/backend/game/developer/create') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.game.admin.developer',  '_sonata_name' => 'admin_backend_game_developer_create',  '_route' => 'admin_backend_game_developer_create',);
            }

            // admin_backend_game_developer_batch
            if ($pathinfo === '/admin/backend/game/developer/batch') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.game.admin.developer',  '_sonata_name' => 'admin_backend_game_developer_batch',  '_route' => 'admin_backend_game_developer_batch',);
            }

            // admin_backend_game_developer_edit
            if (0 === strpos($pathinfo, '/admin/backend/game/developer') && preg_match('#^/admin/backend/game/developer/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.game.admin.developer',  '_sonata_name' => 'admin_backend_game_developer_edit',)), array('_route' => 'admin_backend_game_developer_edit'));
            }

            // admin_backend_game_developer_delete
            if (0 === strpos($pathinfo, '/admin/backend/game/developer') && preg_match('#^/admin/backend/game/developer/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.game.admin.developer',  '_sonata_name' => 'admin_backend_game_developer_delete',)), array('_route' => 'admin_backend_game_developer_delete'));
            }

            // admin_backend_game_developer_show
            if (0 === strpos($pathinfo, '/admin/backend/game/developer') && preg_match('#^/admin/backend/game/developer/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.game.admin.developer',  '_sonata_name' => 'admin_backend_game_developer_show',)), array('_route' => 'admin_backend_game_developer_show'));
            }

            // admin_backend_game_developer_export
            if ($pathinfo === '/admin/backend/game/developer/export') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.game.admin.developer',  '_sonata_name' => 'admin_backend_game_developer_export',  '_route' => 'admin_backend_game_developer_export',);
            }

            // admin_backend_game_publisher_list
            if ($pathinfo === '/admin/backend/game/publisher/list') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.game.admin.publisher',  '_sonata_name' => 'admin_backend_game_publisher_list',  '_route' => 'admin_backend_game_publisher_list',);
            }

            // admin_backend_game_publisher_create
            if ($pathinfo === '/admin/backend/game/publisher/create') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.game.admin.publisher',  '_sonata_name' => 'admin_backend_game_publisher_create',  '_route' => 'admin_backend_game_publisher_create',);
            }

            // admin_backend_game_publisher_batch
            if ($pathinfo === '/admin/backend/game/publisher/batch') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.game.admin.publisher',  '_sonata_name' => 'admin_backend_game_publisher_batch',  '_route' => 'admin_backend_game_publisher_batch',);
            }

            // admin_backend_game_publisher_edit
            if (0 === strpos($pathinfo, '/admin/backend/game/publisher') && preg_match('#^/admin/backend/game/publisher/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.game.admin.publisher',  '_sonata_name' => 'admin_backend_game_publisher_edit',)), array('_route' => 'admin_backend_game_publisher_edit'));
            }

            // admin_backend_game_publisher_delete
            if (0 === strpos($pathinfo, '/admin/backend/game/publisher') && preg_match('#^/admin/backend/game/publisher/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.game.admin.publisher',  '_sonata_name' => 'admin_backend_game_publisher_delete',)), array('_route' => 'admin_backend_game_publisher_delete'));
            }

            // admin_backend_game_publisher_show
            if (0 === strpos($pathinfo, '/admin/backend/game/publisher') && preg_match('#^/admin/backend/game/publisher/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.game.admin.publisher',  '_sonata_name' => 'admin_backend_game_publisher_show',)), array('_route' => 'admin_backend_game_publisher_show'));
            }

            // admin_backend_game_publisher_export
            if ($pathinfo === '/admin/backend/game/publisher/export') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.game.admin.publisher',  '_sonata_name' => 'admin_backend_game_publisher_export',  '_route' => 'admin_backend_game_publisher_export',);
            }

            // admin_backend_game_localizer_list
            if ($pathinfo === '/admin/backend/game/localizer/list') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.game.admin.localizer',  '_sonata_name' => 'admin_backend_game_localizer_list',  '_route' => 'admin_backend_game_localizer_list',);
            }

            // admin_backend_game_localizer_create
            if ($pathinfo === '/admin/backend/game/localizer/create') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.game.admin.localizer',  '_sonata_name' => 'admin_backend_game_localizer_create',  '_route' => 'admin_backend_game_localizer_create',);
            }

            // admin_backend_game_localizer_batch
            if ($pathinfo === '/admin/backend/game/localizer/batch') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.game.admin.localizer',  '_sonata_name' => 'admin_backend_game_localizer_batch',  '_route' => 'admin_backend_game_localizer_batch',);
            }

            // admin_backend_game_localizer_edit
            if (0 === strpos($pathinfo, '/admin/backend/game/localizer') && preg_match('#^/admin/backend/game/localizer/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.game.admin.localizer',  '_sonata_name' => 'admin_backend_game_localizer_edit',)), array('_route' => 'admin_backend_game_localizer_edit'));
            }

            // admin_backend_game_localizer_delete
            if (0 === strpos($pathinfo, '/admin/backend/game/localizer') && preg_match('#^/admin/backend/game/localizer/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.game.admin.localizer',  '_sonata_name' => 'admin_backend_game_localizer_delete',)), array('_route' => 'admin_backend_game_localizer_delete'));
            }

            // admin_backend_game_localizer_show
            if (0 === strpos($pathinfo, '/admin/backend/game/localizer') && preg_match('#^/admin/backend/game/localizer/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.game.admin.localizer',  '_sonata_name' => 'admin_backend_game_localizer_show',)), array('_route' => 'admin_backend_game_localizer_show'));
            }

            // admin_backend_game_localizer_export
            if ($pathinfo === '/admin/backend/game/localizer/export') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.game.admin.localizer',  '_sonata_name' => 'admin_backend_game_localizer_export',  '_route' => 'admin_backend_game_localizer_export',);
            }

            // admin_backend_game_platform_list
            if ($pathinfo === '/admin/backend/game/platform/list') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.game.admin.platform',  '_sonata_name' => 'admin_backend_game_platform_list',  '_route' => 'admin_backend_game_platform_list',);
            }

            // admin_backend_game_platform_create
            if ($pathinfo === '/admin/backend/game/platform/create') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.game.admin.platform',  '_sonata_name' => 'admin_backend_game_platform_create',  '_route' => 'admin_backend_game_platform_create',);
            }

            // admin_backend_game_platform_batch
            if ($pathinfo === '/admin/backend/game/platform/batch') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.game.admin.platform',  '_sonata_name' => 'admin_backend_game_platform_batch',  '_route' => 'admin_backend_game_platform_batch',);
            }

            // admin_backend_game_platform_edit
            if (0 === strpos($pathinfo, '/admin/backend/game/platform') && preg_match('#^/admin/backend/game/platform/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.game.admin.platform',  '_sonata_name' => 'admin_backend_game_platform_edit',)), array('_route' => 'admin_backend_game_platform_edit'));
            }

            // admin_backend_game_platform_delete
            if (0 === strpos($pathinfo, '/admin/backend/game/platform') && preg_match('#^/admin/backend/game/platform/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.game.admin.platform',  '_sonata_name' => 'admin_backend_game_platform_delete',)), array('_route' => 'admin_backend_game_platform_delete'));
            }

            // admin_backend_game_platform_show
            if (0 === strpos($pathinfo, '/admin/backend/game/platform') && preg_match('#^/admin/backend/game/platform/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.game.admin.platform',  '_sonata_name' => 'admin_backend_game_platform_show',)), array('_route' => 'admin_backend_game_platform_show'));
            }

            // admin_backend_game_platform_export
            if ($pathinfo === '/admin/backend/game/platform/export') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.game.admin.platform',  '_sonata_name' => 'admin_backend_game_platform_export',  '_route' => 'admin_backend_game_platform_export',);
            }

            // admin_backend_game_genre_list
            if ($pathinfo === '/admin/backend/game/genre/list') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.game.admin.genre',  '_sonata_name' => 'admin_backend_game_genre_list',  '_route' => 'admin_backend_game_genre_list',);
            }

            // admin_backend_game_genre_create
            if ($pathinfo === '/admin/backend/game/genre/create') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.game.admin.genre',  '_sonata_name' => 'admin_backend_game_genre_create',  '_route' => 'admin_backend_game_genre_create',);
            }

            // admin_backend_game_genre_batch
            if ($pathinfo === '/admin/backend/game/genre/batch') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.game.admin.genre',  '_sonata_name' => 'admin_backend_game_genre_batch',  '_route' => 'admin_backend_game_genre_batch',);
            }

            // admin_backend_game_genre_edit
            if (0 === strpos($pathinfo, '/admin/backend/game/genre') && preg_match('#^/admin/backend/game/genre/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.game.admin.genre',  '_sonata_name' => 'admin_backend_game_genre_edit',)), array('_route' => 'admin_backend_game_genre_edit'));
            }

            // admin_backend_game_genre_delete
            if (0 === strpos($pathinfo, '/admin/backend/game/genre') && preg_match('#^/admin/backend/game/genre/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.game.admin.genre',  '_sonata_name' => 'admin_backend_game_genre_delete',)), array('_route' => 'admin_backend_game_genre_delete'));
            }

            // admin_backend_game_genre_show
            if (0 === strpos($pathinfo, '/admin/backend/game/genre') && preg_match('#^/admin/backend/game/genre/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.game.admin.genre',  '_sonata_name' => 'admin_backend_game_genre_show',)), array('_route' => 'admin_backend_game_genre_show'));
            }

            // admin_backend_game_genre_export
            if ($pathinfo === '/admin/backend/game/genre/export') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.game.admin.genre',  '_sonata_name' => 'admin_backend_game_genre_export',  '_route' => 'admin_backend_game_genre_export',);
            }

            // sonata_user_admin_security_login
            if ($pathinfo === '/admin/login') {
                return array (  '_controller' => 'Sonata\\UserBundle\\Controller\\AdminSecurityController::loginAction',  '_route' => 'sonata_user_admin_security_login',);
            }

            // sonata_user_admin_security_check
            if ($pathinfo === '/admin/login_check') {
                return array (  '_controller' => 'Sonata\\UserBundle\\Controller\\AdminSecurityController::checkAction',  '_route' => 'sonata_user_admin_security_check',);
            }

            // sonata_user_admin_security_logout
            if ($pathinfo === '/admin/logout') {
                return array (  '_controller' => 'Sonata\\UserBundle\\Controller\\AdminSecurityController::logoutAction',  '_route' => 'sonata_user_admin_security_logout',);
            }

            // revert_changeset
            if (0 === strpos($pathinfo, '/admin/revert') && preg_match('#^/admin/revert/(?P<entity>[^/]+?)/(?P<id>[^/]+?)/(?P<version>[^/]+?)$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Backend\\UserBundle\\Controller\\RevertController::revertAction',)), array('_route' => 'revert_changeset'));
            }

        }

        // homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }
            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::dashboardAction',  '_route' => 'homepage',);
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
