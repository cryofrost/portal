<?php

namespace Backend\GameBundle\Admin;

/**
 * Description of GameAdmin
 *
 * @author Cryo-Win7-32
 */
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Knp\Menu\ItemInterface as MenuItemInterface;


class GenreAdmin extends Admin
{
	private $fields = array(
		'id' => array('label' => 'Идентификатор'),
		'alias' => array('label' => 'Псевдоним'),
		'name' => array('label' => 'Название')
	);
	
    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
		foreach($this->fields as $key => $value)
			if($key == 'id')
				continue;
			else
				$formMapper
					->add($key, null, array('label' => $value['label'], 'mulitple' => true, 'by_reference' => false));
    }
	
	/**
     * Конфигурация отображения записи
     *
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     * @return void
     */	
    protected function configureShowFields(ShowMapper $showMapper)
    {
		foreach($this->fields as $key => $value)
			$showMapper->add($key, null, array('label' => $value['label']));
		
    }
	
	/**
     * Конфигурация списка записей
     *
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
		foreach($this->fields as $key => $value)
			$listMapper
				 ->addIdentifier($key, null, array('label' => $value['label']));

    }
	
}

?>