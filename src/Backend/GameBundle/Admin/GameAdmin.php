<?php

namespace Backend\GameBundle\Admin;

/**
 * Description of GameAdmin
 *
 * @author Cryo-Win7-32
 */
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Knp\Menu\ItemInterface as MenuItemInterface;

class GameAdmin extends Admin
{
	private $fields = array(
		'id' => array('label' => 'Идентификатор'),
		'alias' => array('label' => 'Псевдоним'),
		'name' => array('label' => 'Название'),
		'developer' => array('label' => 'Разработчик'),
		'publisher' => array('label' => 'Издатель'),
		'localizer' => array('label' => 'Локализатор'),
		'platform' => array('label' => 'Платформа'),
		'genre' => array('label' => 'Жанр'),
		'local_name' => array('label' => 'Местное название'),
		'release_date' => array('label' => 'Дата релиза'),
		'site' => array('label' => 'Сайт'),
		'local_site' => array('label' => 'Местный сайт'),
		'description' => array('label' => 'Описание'),
		'short_description' => array('label' => 'Короткое описание')
	);

	
    /**
     * Конфигурация отображения записи
     *
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     * @return void
     */	
    protected function configureShowFields(ShowMapper $showMapper)
    {
		foreach($this->fields as $key => $value)
			$showMapper->add($key, null, array('label' => $value['label']));
		
    }
	

    /**
     * Конфигурация формы редактирования записи
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
		foreach($this->fields as $key => $value)
			switch($key):
				case 'developer': 
				case 'publisher': 
				case 'localizer': 
					$this->addRelatedItemsByAlias($formMapper, $key); 
				break;
				
				case 'genre':
				case 'platform':
					#$formMapper
					#->add($key, 'sonata_type_model',  array(
						#'expanded' => true,
						#'property' => 'game',
						#'multiple' => true,
						#'by_reference' => false
                    #));
				break;
			
				case 'id': 
					continue;
			
				default:
					$formMapper->add($key, null, array('label' => $value['label']));
			endswitch;
    }
	
	/**
	 * @todo  trow exception on wrong alias
	 * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
	 * @param type $alias
	 * @return boolean
	 */
	private function addRelatedItemsByAlias(FormMapper $formMapper, $alias)
	{
		if(!array_key_exists($alias, $this->fields))
			return false;
		
		$formMapper
			->add($alias, 'sonata_type_model', array('label' => $this->fields[$alias]['label']), array('edit' => 'list','inline' => 'table'));
	}
	

    /**
     * Конфигурация списка записей
     *
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id')
                ->addIdentifier('alias', null, array('label' => 'Заголовок'));

    }

    /**
     * Поля, по которым производится поиск в списке записей
     *
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
			->add('alias', null, array('label' => 'Заголовок'));
    }

    /**
     * Конфигурация левого меню при отображении и редатировании записи
     *
     * @param \Knp\Menu\ItemInterface $menu
     * @param $action
     * @param null|\Sonata\AdminBundle\Admin\Admin $childAdmin
     *
     * @return void
     */
    protected function configureSideMenu(MenuItemInterface $menu, $action, Admin $childAdmin = null)
    {
        $menu->addChild(
            ($action == 'edit' ? 'Просмотр игры' : 'Редактирование игры'),
            array('uri' => $this->generateUrl(
                $action == 'edit' ? 'show' : 'edit', array('id' => $this->getRequest()->get('id'))))
        );
    }
}

?>