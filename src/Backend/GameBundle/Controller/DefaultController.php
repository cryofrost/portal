<?php

namespace Backend\GameBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DefaultController extends Controller
{
    
    public function indexAction($name)
    {
        return $this->render('BackendGameBundle:Default:index.html.twig', array('name' => $name));
    }
}
