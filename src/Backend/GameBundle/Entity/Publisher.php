<?php

namespace Backend\GameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Backend\GameBundle\Entity\Publisher
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Publisher
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @var string $alias
	 * 
     * @ORM\Column(type="string", length="255", unique="true")
     */
    private $alias;
	
	/**
	 * @var string $name
	 * 
     * @ORM\Column(type="string", length="255")
     */
    private $name;
	
	/**
     * @ORM\OneToMany(targetEntity="Publisher", mappedBy="game", cascade={"persist", "remove"})
     */
    private $game;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    public function __construct()
    {
        $this->game = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set alias
     *
     * @param string $alias
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    }

    /**
     * Get alias
     *
     * @return string 
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add game
     *
     * @param Backend\GameBundle\Entity\Publisher $game
     */
    public function addPublisher(\Backend\GameBundle\Entity\Publisher $game)
    {
        $this->game[] = $game;
    }

    /**
     * Get game
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getGame()
    {
        return $this->game;
    }
}