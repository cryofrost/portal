<?php

namespace Backend\GameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Backend\GameBundle\Entity\Game
 *
 * @ORM\Table(name="game")
 * @ORM\Entity
 */
class Game
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @var string $alias
	 * 
     * @ORM\Column(type="string", length="255", unique="true")
     */
    private $alias;
	
	/**
	 * @var string $name
	 * 
     * @ORM\Column(type="string", length="255")
     */
    private $name;
	
	/**
	 * @var string $local_name
	 * 
     * @ORM\Column(type="string", length="255", nullable="true")
     */
    private $local_name;
	
	/**
	 * @var string $release_date
	 * 
     * @ORM\Column(type="date")
     */
	private $release_date;
	
	/**
	 * @var string $site
	 * 
     * @ORM\Column(type="string", length="255")
     */
    private $site;
	
	/**
	 * @var string $site
	 * 
     * @ORM\Column(type="string", length="255", nullable="true")
     */
    private $local_site;
	
	/**
     * @ORM\ManyToOne(targetEntity="Developer", inversedBy="game")
     */
    private $developer;
	
	/**
	 * @var string $publisher
	 * 
     * @ORM\ManyToOne(targetEntity="Publisher", inversedBy="game")
     */
    private $publisher;
	
	/**
	 * @var string $localizer
	 * 
     * @ORM\ManyToOne(targetEntity="Localizer", inversedBy="game")
     */
    private $localizer;
	

	/**
     * @ORM\ManyToMany(targetEntity="Genre", inversedBy="game")
     * @ORM\JoinTable(name="game_genre_relation")
     */
    private $genre;
	
	/**
     * @ORM\ManyToMany(targetEntity="Platform", inversedBy="game")
	 * @ORM\JoinTable(name="game_platform_relation")
     */
    private $platform;
	
	
	/**
	 * @var string $description
	 * 
     * @ORM\Column(type="text")
     */
    private $description;
	
	/**
	 * @var string $short_description
	 * 
     * @ORM\Column(type="text")
     */
    private $short_description;

	
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
	
    /**
     * Set alias
     *
     * @param string $alias
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    }

    /**
     * Get alias
     *
     * @return string 
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set local_name
     *
     * @param string $localName
     */
    public function setLocalName($localName)
    {
        $this->local_name = $localName;
    }

    /**
     * Get local_name
     *
     * @return string 
     */
    public function getLocalName()
    {
        return $this->local_name;
    }

    /**
     * Set release_date
     *
     * @param date $releaseDate
     */
    public function setReleaseDate($releaseDate)
    {
        $this->release_date = $releaseDate;
    }

    /**
     * Get release_date
     *
     * @return date 
     */
    public function getReleaseDate()
    {
        return $this->release_date;
    }

    /**
     * Set site
     *
     * @param string $site
     */
    public function setSite($site)
    {
        $this->site = $site;
    }

    /**
     * Get site
     *
     * @return string 
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set local_site
     *
     * @param string $localSite
     */
    public function setLocalSite($localSite)
    {
        $this->local_site = $localSite;
    }

    /**
     * Get local_site
     *
     * @return string 
     */
    public function getLocalSite()
    {
        return $this->local_site;
    }

    /**
     * Set description
     *
     * @param text $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return text 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set short_description
     *
     * @param text $shortDescription
     */
    public function setShortDescription($shortDescription)
    {
        $this->short_description = $shortDescription;
    }

    /**
     * Get short_description
     *
     * @return text 
     */
    public function getShortDescription()
    {
        return $this->short_description;
    }
    
    /**
     * Set developper
     *
     * @param string $developper
     */
    public function setDevelopper($developper)
    {
        $this->developper = $developper;
    }

    /**
     * Get developper
     *
     * @return string 
     */
    public function getDevelopper()
    {
        return $this->developper;
    }

    /**
     * Set publisher
     *
     * @param string $publisher
     */
    public function setPublisher($publisher)
    {
        $this->publisher = $publisher;
    }

    /**
     * Get publisher
     *
     * @return string 
     */
    public function getPublisher()
    {
        return $this->publisher;
    }

    /**
     * Set localizer
     *
     * @param string $localizer
     */
    public function setLocalizer($localizer)
    {
        $this->localizer = $localizer;
    }

    /**
     * Get localizer
     *
     * @return string 
     */
    public function getLocalizer()
    {
        return $this->localizer;
    }

    /**
     * Add genre
     *
     * @param Backend\GameBundle\Entity\Genre $genre
     */
    public function addGenre(\Backend\GameBundle\Entity\Genre $genre)
    {
        $this->genre[] = $genre;
    }

    /**
     * Get genre
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * Add platform
     *
     * @param Backend\GameBundle\Entity\Platform $platform
     */
    public function addPlatform(\Backend\GameBundle\Entity\Platform $platform)
    {
        $this->platform[] = $platform;
    }

    /**
     * Get platform
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * Add developer
     *
     * @param Backend\GameBundle\Entity\Developer $developer
     */
    public function addDeveloper(\Backend\GameBundle\Entity\Developer $developer)
    {
        $this->developer[] = $developer;
    }

    /**
     * Get developer
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getDeveloper()
    {
        return $this->developer;
    }

    /**
     * Add publisher
     *
     * @param Backend\GameBundle\Entity\Publisher $publisher
     */
    public function addPublisher(\Backend\GameBundle\Entity\Publisher $publisher)
    {
        $this->publisher[] = $publisher;
    }

    /**
     * Add localizer
     *
     * @param Backend\GameBundle\Entity\Localizer $localizer
     */
    public function addLocalizer(\Backend\GameBundle\Entity\Localizer $localizer)
    {
        $this->localizer[] = $localizer;
    }
    public function __construct()
    {
        $this->genre = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set developer
     *
     * @param Backend\GameBundle\Entity\Developer $developer
     */
    public function setDeveloper(\Backend\GameBundle\Entity\Developer $developer)
    {
        $this->developer = $developer;
    }

    /**
     * Set platform
     *
     * @param Backend\GameBundle\Entity\Platform $platform
     */
    public function setPlatform(\Backend\GameBundle\Entity\Platform $platform)
    {
        $this->platform = $platform;
    }
}