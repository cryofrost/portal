<?php

namespace Backend\GameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Backend\GameBundle\Entity\Genre
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Genre
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @var string $alias
	 * 
     * @ORM\Column(type="string", length="255", unique="true")
     */
    private $alias;
	
	/**
	 * @var string $name
	 * 
     * @ORM\Column(type="string", length="255")
     */
    private $name;
	

	/**
     * @ORM\ManyToMany(targetEntity="Game", mappedBy="genre")
     */
	private $game;
	

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set alias
     *
     * @param string $alias
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    }

    /**
     * Get alias
     *
     * @return string 
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
	
    public function __construct()
    {
        $this->game = new \Doctrine\Common\Collections\ArrayCollection();
    }
	
    
    /**
     * Add game
     *
     * @param Backend\GameBundle\Entity\Game $game
     */
    public function addGame(\Backend\GameBundle\Entity\Game $game)
    {
        $this->game[] = $game;
    }

    /**
     * Get game
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getGame()
    {
        return $this->game;
    }
}