<?php

namespace Backend\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of RevertController
 *
 * @author Cryo-Win7-32
 */
class RevertController extends Controller {

    /**
     * @return \Symfony\Bundle\FrameworkBundle\Controller\Response
     */
    public function revertAction($entity, $id, $version) {
        $em = $this->getDoctrine()->getEntityManager();
        $repo = $em->getRepository('Gedmo\Loggable\Entity\LogEntry');
        $uow = $em->find('Backend\UserBundle\Entity\\'.$entity, $id);
        $repo->revert($uow, --$version);//revert the state of entity to previous version
        $em->persist($uow);
        $em->flush();
        return $this->render($this->container->get('sonata.admin.pool')->getTemplate('dashboard'), array(
            'base_template'   => $this->container->get('sonata.admin.pool')->getTemplate('layout'),
            'admin_pool'      => $this->container->get('sonata.admin.pool'),
            'blocks'          => $this->container->getParameter('sonata.admin.configuration.dashboard_blocks')
        ));
    }

}

?>