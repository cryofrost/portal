<?php

namespace Backend\UserBundle\Listener;

use Gedmo\Loggable\LoggableListener;
use Gedmo\Mapping\MappedEventSubscriber;
use Gedmo\Loggable\Mapping\Event\LoggableAdapter;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\EventArgs;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;


class UserLoggableListener extends LoggableListener {

     /**
     * Looks for loggable objects being inserted or updated
     * to write them in logfile
     *
     * @param EventArgs $args
     * @return void
     */
     public function onFlush(EventArgs $eventArgs){
         //create a standart log entry to DB
        $ea = $this->getEventAdapter($eventArgs);
        $om = $ea->getObjectManager();
        $uow = $om->getUnitOfWork();

        foreach ($ea->getScheduledObjectInsertions($uow) as $object) {
            $this->createLogEntry(self::ACTION_CREATE, $object, $ea);
        }
        foreach ($ea->getScheduledObjectUpdates($uow) as $object) {
            $this->createLogEntry(self::ACTION_UPDATE, $object, $ea);
        }
        foreach ($ea->getScheduledObjectDeletions($uow) as $object) {
            $this->createLogEntry(self::ACTION_REMOVE, $object, $ea);
        }
     }

     /**
     * Create a new Log instance and send log entry to text log
     *
     * @param string $action
     * @param object $object
     * @param LoggableAdapter $ea
     * @return void
     */
    private function createLogEntry($action, $object, LoggableAdapter $ea)
    {
        $om = $ea->getObjectManager();
        $meta = $om->getClassMetadata(get_class($object));
        if ($config = $this->getConfiguration($om, $meta->name)) {
            $logEntryClass = $this->getLogEntryClass($ea, $meta->name);
            $logEntry = new $logEntryClass;

            $logEntry->setAction($action);
            $logEntry->setUsername($this->username);
            $logEntry->setObjectClass($meta->name);
            $logEntry->setLoggedAt();

            // check for the availability of the primary key
            $identifierField = $ea->getSingleIdentifierFieldName($meta);
            $objectId = $meta->getReflectionProperty($identifierField)->getValue($object);
            if (!$objectId && $action === self::ACTION_CREATE) {
                $this->pendingLogEntryInserts[spl_object_hash($object)] = $logEntry;
            }
            $uow = $om->getUnitOfWork();
            $logEntry->setObjectId($objectId);
            if ($action !== self::ACTION_REMOVE && isset($config['versioned'])) {
                $newValues = array();
                foreach ($ea->getObjectChangeSet($uow, $object) as $field => $changes) {
                    if (!in_array($field, $config['versioned'])) {
                        continue;
                    }
                    $value = $changes[1];
                    if ($meta->isSingleValuedAssociation($field) && $value) {
                        $oid = spl_object_hash($value);
                        $value = $ea->extractIdentifier($om, $value, false);
                        if (!is_array($value)) {
                            $this->pendingRelatedObjects[$oid][] = array(
                                'log' => $logEntry,
                                'field' => $field
                            );
                        }
                    }
                    $newValues[$field] = $value;
                }
                $logEntry->setData($newValues);
            }
            $version = 1;
            $logEntryMeta = $om->getClassMetadata($logEntryClass);
            if ($action !== self::ACTION_CREATE) {
                $version = $ea->getNewVersion($logEntryMeta, $object);
            }
            $logEntry->setVersion($version);

            $om->persist($logEntry);
            $uow->computeChangeSet($logEntryMeta, $logEntry);
            $this->logEntryToLogFile($logEntry);
        }
    }
    /**
     *  Write log entry to text log
     */
    private function logEntryToLogFile($logEntry)
        {
        $message = 'User "' . $logEntry->getUsername()
                    . '" performed action "' . $logEntry->getAction()
                    . '" on user with ID=' . $logEntry->getObjectId();

        $changeset = $logEntry->getData();
        $message .= '. Changeset: ' . $this->f2s($changeset);

        $version = $logEntry->getVersion();

        if ($version !==1){
        $message .= '. To revert this, invoke request to /revert/User/'
                    . $logEntry->getObjectId()
                    . '/' . $version;
        }
        
        // create new log channel with custom handler
        $log = new Logger('UserUpdate');
        $container = \Backend\UserBundle\BackendUserBundle::getContainer();
        $logFilename = $container->get('kernel')->getLogDir() . '/';
        $logFilename .= $container->get('kernel')->getEnvironment() . '_user.log';
        $log->pushHandler(new StreamHandler($logFilename, Logger::INFO));
        // add record to the log
        $log->addInfo($message);
    }

    /*
     * Converts array to text string
     */
    private function f2s($array){
        $str = '';
        foreach($array as $key=>$value){
            if (is_array($value)){
                $str .= $key . ':' . $this->f2s($value);
            }
            else
            $str .= $key . ':' . $value;
        }
        return $str;
    }
}

?>