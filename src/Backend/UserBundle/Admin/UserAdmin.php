<?php

namespace Backend\UserBundle\Admin;

/**
 * Description of GameAdmin
 *
 * @author Cryo-Win7-32
 */
use Sonata\UserBundle\Admin\Model\UserAdmin as BaseUserAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Knp\Menu\ItemInterface as MenuItemInterface;

class UserAdmin extends BaseUserAdmin
{
    /**
     * Конфигурация отображения записи
     *
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     * @return void
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
                ->add('id', null, array('label' => 'Идентификатор'))
                ->add('firstname', null, array('label' => 'Имя'))
                ->add('lastname', null, array('label' => 'Фамилия'))
                ->add('email', null, array('label' => 'Электропочта'))
                ->add('biography', null, array('label' => 'Описание'))
            ;
    }

    /**
     * Конфигурация формы редактирования записи
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Основные')
                ->add('username', null, array('label' => 'Ник'))
                ->add('plainPassword', 'text', array('label' => 'Пароль'))
                ->add('email')
            ->end()
            ->with('Группы')
                ->add('group', 'sonata_type_model', array('required' => false,
                                                          'multiple' => false,
                                                          'label' => 'Группа'))
            ->end()
            ->with('Личные')
                ->add('firstname', null, array('label' => 'Имя'))
                ->add('lastname', null, array('label' => 'Фамилия'))
                ->add('biography', null, array('label' => 'Описание'))
            ->end()
                ->setHelps(array(
                                'firstname' => 'Имя пользователя',
                                'lastname' => 'Фамилия пользователя',
                                'biography' => 'Описание пользователя'
                               ));

            if (!$this->getSubject()->hasRole('ROLE_SUPER_ADMIN')) {
                $formMapper->with('Management')
                    ->add('roles', 'sonata_security_roles', array(
                        'expanded' => false,
                        'multiple' => true,
                        'required' => false
                    ))
                    ->add('locked', null, array('required' => false))
                    ->add('expired', null, array('required' => false))
                    ->add('enabled', null, array('required' => false))
                    ->add('credentialsExpired', null, array('required' => false))
                ->end();

            $formMapper
            ->with('Security')
                ->add('token', null, array('required' => false))
                ->add('twoStepVerificationCode', null, array('required' => false))
            ->end();
            }
    }

    /**
     * Конфигурация списка записей
     *
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id', null, array('label' => 'ID'))
                ->addIdentifier('username', null, array('label' => 'Ник'))
                ->addIdentifier('firstname', null, array('label' => 'Имя'))
                ->addIdentifier('lastname', null, array('label' => 'Фамилия'))
                ->addIdentifier('email', null, array('label' => 'Электропочта'));
    }

    /**
     * Поля, по которым производится поиск в списке записей
     *
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('username', null, array('label' => 'Ник'))
                ->add('email', null, array('label' => 'Электропочта'))
                ->add('groups', null, array('label' => 'Группы'))
            ;
    }
}

?>