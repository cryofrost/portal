<?php

namespace Backend\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sonata\UserBundle\Entity\BaseGroup as BaseGroup;

class Group extends BaseGroup
{
    /**
     * @var integer $id
     */
    protected $id;


    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="user")
     */
    private $users;

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get users
     *
     * @return $users
     */
    public function getUsers()
    {
        return $this->users;
    }
}